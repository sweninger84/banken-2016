/**
 * @brief: header file for request struct
 * @author: Sabine Weninger
 * @date: 14.12.2016
 */

#ifndef REQUEST
#define REQUEST

#define ACCOUNT_NUMBER_LENGTH (7)
#define OPERATION_LENGTH (10)

typedef struct {
	uint8_t account_number[ACCOUNT_NUMBER_LENGTH];
	char operation[OPERATION_LENGTH];
	uint8_t other_account[ACCOUNT_NUMBER_LENGTH];
	long value;
	pid_t pid;
} request;

#endif
