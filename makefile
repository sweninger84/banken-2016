#author: Sabine Weninger
#about: Makefile for Banken assignment

CC = gcc
DEFS = - D_XOPEN_SOURCE =500 - D_BSD_SOURCE
DEPS = account.h request.h
CFLAGS = -Wall -g -std=c99 -pedantic $( DEFS )
OBJECTFILES = bankclient.o bankserver.o

all : bankclient bankserver

bankclient : bankclient.o
	$(CC) -g -o $@ $^ -pthread -lrt

bankserver : bankserver.o account.o
	$(CC) -g -o $@ $^ -pthread -lrt

%.o: %.c $(DEPS)
	$(CC) $( CFLAGS ) -c -o $@ $<

clean :
	rm -f bankclient.o bankserver.o account.o bankclient bankserver
