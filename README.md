#Simple command line Bank Application written in C
####(University Exercise to learn POSIX Semaphores and Shared Memory)

Consists of a Bankserver and a Bankclient Application.
Can be built using "make all" on a Linux operating system.
-----------------------------------------------------

Bankserver Usage: **bankserver [ACCOUNT_LIST [OUT_ACCOUNT_LIST]]**

where ACCOUNT_LIST is a .csv file containing all accounts and OUT_ACCOUNT_LIST is a file name where changed accounts will be saved to. If no ACCOUNT_LIST is provided, accounts 
can be typed in after the application starts (format: <7 digit number>;<balance>). Ctrl+D stops input mode. For convenience, the file "accounts.csv" provides a number of test accounts.


Bankclient Usage: **bankclient ACCOUNT_NR**

where ACCOUNT_NR is the account number of this client.

-----------------------------------------------------

List of Operations for the Client:

* **!balance** Prints out the current balance for this account

* **!deposit <amount>** Adds <amount> to this account balance

* **!withdraw <amount>** Subtracts <amount> from this account balance. Prints an error if not enough money is available.

* **!transfer <amount> <account-number of receiving account>**  Transfers <amount> from this account to the given account. Prints an error if not enough money is available.
all amounts are of type Long.


Bankserver and Bankclient communicate via POSIX Shared Memory. Only one client can be logged in per account at one time. Each client tries to login upon start of the application. Semaphores make sure and all requests
are executed one after the other, thereby preventing invalid states on the shared memory.
