/**
 * @brief: server implementation of the banken assignment
 * @author: Sabine Weninger
 * @date: 14.12.2016
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <limits.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include "account.h"
#include "request.h"



/* === Constants === */

#define ACCOUNT_LENGTH (100)
#define INITIAL_SIZE (5)
#define RESPONSE_SIZE (50)
#define SHM_NAME ("/1412-shm")
#define SEM_MUTEX_NAME ("/1412-sem-mutex")
#define SEM_REQUEST_SIGNAL_NAME ("/1412_sem-request-signal")
#define SEM_RESPONSE_SIGNAL_NAME ("/1412_sem-response-signal")


/* === Macros === */

#ifdef ENDEBUG
#define DEBUG(...) do { fprintf(stderr, __VA_ARGS__); } while(0)
#else
#define DEBUG(...)
#endif

/* === Global Struct === */
struct shared_memory{
    request client_request;
    char response_buffer[RESPONSE_SIZE];
};

/* === Global Variables === */
static char *progname = "bankserver";
static char *account_path;
static char *out_account_path;
volatile sig_atomic_t exit_flag = 0;
static struct shared_memory *shm_ptr;
static sem_t *mutex_sem, *request_signal_sem, *response_signal_sem;



/** Accounts list */
static account **account_list;
static uint16_t account_list_length = 0;
static uint16_t account_list_max_length = 0;


/**
* @brief Uses getopt to make sure no options were given
* @param argc the argument counter, argv[] the argument array
*/
static void parse_args(int argc, char *argv[]){
	int opt;

	while ((opt = getopt(argc, argv, "")) != -1) {
		switch (opt) {
			case '?':
				(void) fprintf(stderr, "Usage: %s [ACCOUNT_LIST [OUT_ACCOUNT_LIST]]\n", argv[0]);
				exit(EXIT_FAILURE);
				break;
			default: /* '?' */
				(void) fprintf(stderr, "Usage: %s [ACCOUNT_LIST [OUT_ACCOUNT_LIST]]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	if (argc - optind > 2) {
		(void) fprintf(stderr, "Usage: %s [ACCOUNT_LIST [OUT_ACCOUNT_LIST]]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	progname = argv[0];
	if(argc - optind >= 1){
		account_path = argv[optind];
	}
	if(argc - optind > 1){
		out_account_path = argv[optind + 1];
	}
}

/**
* @brief Frees all resources, unlinks semaphores etc.
*/
void free_resources(){
    printf("Freeing resources!\n");
	int error = 0;

	if(account_list != NULL){
		int i;
		for(i = 0; i < account_list_length; ++i){
			if(account_list[i] != NULL){
				free(account_list[i]);
			}
		}
		free(account_list);
	}

	//unlink all semaphores
	if (sem_unlink (SEM_MUTEX_NAME) == -1) {
        (void) fprintf(stderr, "%s: %s\n", progname, "sem_unlink");
		error = 1;
    }
    if (sem_unlink (SEM_REQUEST_SIGNAL_NAME) == -1) {
        (void) fprintf(stderr, "%s: %s\n", progname, "sem_unlink");
		error = 1;
    }
    if (sem_unlink (SEM_RESPONSE_SIGNAL_NAME) == -1) {
        (void) fprintf(stderr, "%s: %s\n", progname, "sem_unlink");
		error = 1;
    }
	//unlink shared memory
    if (munmap (shm_ptr, sizeof (struct shared_memory)) == -1){
        (void) fprintf(stderr, "%s: %s\n", progname, "munmap");
		error = 1;
    }
	if (shm_unlink (SHM_NAME) == -1){
		(void) fprintf(stderr, "%s: %s\n", progname, "shm_unlink");
		error = 1;
	}
	if(error){
		exit(EXIT_FAILURE);
	}
}

/**
* @brief Prints error message, calls free_resources and exits with FAILURE
* @param message: the error message to be printed
*/
void bail_out(char *message){
	(void) fprintf(stderr, "%s: %s\n", progname, message);
	(void) free_resources();
	exit(EXIT_FAILURE);
}

/**
* @brief sets the exit_flag to 1 (true), when a SIGINT or SIGTERM Signal is received
*/
void signal_handler() {
  exit_flag = 1;
}

/**
* @brief takes a file stream, and reads in the accounts into account_list
* @param stream: a pointer to the file stream (stdin, or file)
*/
void read_accounts(FILE *stream) {
	char *line;
	account *acc;
	account **tmp;

	line = malloc(ACCOUNT_LENGTH * sizeof(char));
	if(line == NULL){
		(void) free_resources();
		exit(EXIT_FAILURE);
	}
	while(fgets(line, (ACCOUNT_LENGTH * sizeof(char)), stream) != NULL){
		if(account_list_length >= account_list_max_length){
			//Element does not fit anymore, more memory needs to be allocated (grows in steps of INITIAL_SIZE)
			account_list_max_length = account_list_max_length + INITIAL_SIZE;
			//printf("Reallocating memory\n");
			tmp = realloc(account_list, account_list_max_length * sizeof(account *));
			if(tmp == NULL){
				free(line);
				bail_out("No more memory available.");
			}
			account_list = tmp;
		}
		//enough memory allocated, we can parse the account
		acc = parse_account(line);
		if(acc == NULL){
			fprintf(stderr, "Malformed account.\n");
		}else{
            account_list[account_list_length] = acc;
    		++account_list_length;
        }
	}
	//We don't need line anymore, can be freed
	free(line);
}

void write_account_list(){
    int i;
    FILE *file;
    if(out_account_path != NULL){
		if((file = fopen(out_account_path, "w")) == NULL){
			bail_out("Could not open out account file");
		}
    }else if(account_path != NULL){
        if((file = fopen(account_path, "w")) == NULL){
			bail_out("Could not open out account file");
		}
    }else{
        file = stdout;
    }
    for(i = 0; i < account_list_length; ++i){
        print_account(account_list[i], file);
    }
}

/**
* @brief loops through account_list and returns the account with the correct account_number
* @param account_number: pointer to the account_number
*/
account *find_account(uint8_t *account_number){
	int i;
	account *answer = NULL;
	for(i = 0; i < account_list_length; ++i){
		if(account_numbers_equal(account_list[i]->account_number, account_number) == 1){
			answer = account_list[i];
		}
	}
	return answer;
}

/**
* @brief takes the client_request, executes it and writes the response into the response buffer
* @param client_request: pointer to the client_request struct; response: pointer to the response buffer
*/
void fulfill_request(request *client_request, char* response){
	account *client_account, *secondary_account;
	client_account = find_account(client_request->account_number);
	if(client_account == NULL){
		strcpy(response, "Cannot log in to this account.");
		return;
	}
	if(strcmp(client_request->operation, "!login") == 0){
		if(client_account->pid == 0){
            client_account->pid = client_request->pid;
			strcpy(response, "Successfully logged in.");
		}else if(client_account->pid == client_request->pid){
			strcpy(response, "Already logged in.");
		}else{
            strcpy(response, "Cannot log in to this account.");
        }
		return;
	}
	if(client_account->pid == 0 || client_account->pid != client_request->pid){
		strcpy(response, "Not logged in.");
		return;
	}
	if(strcmp(client_request->operation, "!logout") == 0){
		client_account->pid = 0;
		strcpy(response, "Successfully logged out.");
	}
	else if(strcmp(client_request->operation, "!balance") == 0){
		sprintf(response, "Balance: %ld", client_account->balance);
	}
	else if(strcmp(client_request->operation, "!withdraw") == 0){
		if(client_request->value <= 0){
			strcpy(response, "Cannot withdraw negative/zero value");
		}else if(client_account->balance - client_request->value < 0){
			strcpy(response, "Not enough money on account");
		}else{
			client_account->balance -= client_request->value;
			strcpy(response, "Sucessfully withdrew money from account");
		}
	}
	else if(strcmp(client_request->operation, "!deposit") == 0){
		if(client_request->value <= 0){
			strcpy(response, "Cannot deposit negative/zero value");
		}else{
			client_account->balance += client_request->value;
			strcpy(response, "Sucessfully deposited money on account");
		}
	}
	else if(strcmp(client_request->operation, "!transfer") == 0){
		if(client_request->value <= 0){
			strcpy(response, "Cannot transfer negative/zero value");
		}else if(client_account->balance - client_request->value < 0){
			strcpy(response, "Not enough money on account");
		}else{
			secondary_account = find_account(client_request->other_account);
			if(secondary_account == NULL){
				strcpy(response, "Receiving account not in list");
			}else{
				client_account->balance -= client_request->value;
				secondary_account->balance += client_request->value;
				strcpy(response, "Sucessfully transferred money");
			}
		}
	}
	else{
		strcpy(response, "Unknown operation");
	}
}

/**
* @brief Program entry point. Parses the program arguments. Creates request and resposne shared memories, creates semaphores.
*        Waits for a client_request, executes it and writes the response into the response shared memory.
* @param arc: argument counter; argv: pointer to argument list
*/
int main(int argc, char *argv[]){
	int i, fd_shm;
	char *response;
	(void) parse_args(argc, argv);

	/* setup signal handlers */
    const int signals[] = {SIGINT, SIGTERM};
    struct sigaction s;
    s.sa_handler = signal_handler;
    s.sa_flags   = 0;
    if(sigfillset(&s.sa_mask) < 0) {
        bail_out("sigfillset");
    }
    for(i = 0; i < (sizeof(signals) / sizeof(int)); i++) {
        if (sigaction(signals[i], &s, NULL) < 0) {
            bail_out("sigaction");
        }
    }

	/* read in accounts */
	if(account_path == NULL){
		read_accounts(stdin);
	}else{
		FILE *file;
		if((file = fopen(account_path, "r")) == NULL){
			bail_out("Could not open account list");
		}
		read_accounts(file);
	}

	for(i = 0; i < account_list_length; ++i){
		print_account(account_list[i], stdout);
	}

	// Create request shared memory
    if ((fd_shm = shm_open(SHM_NAME, O_RDWR | O_CREAT, 0660)) == -1){
		bail_out("shm_open error");
	}
	if (ftruncate (fd_shm, sizeof (request)) == -1){
		bail_out("ftruncate error");
	}
	if ((shm_ptr = mmap (NULL, sizeof (struct shared_memory), PROT_READ | PROT_WRITE, MAP_SHARED,
            fd_shm, 0)) == MAP_FAILED){
				bail_out("mmap error");
			}

	//Get semaphores
	//  Mutual exclusion semaphore. Makes sure only one request is printed to shared memory. All others have to wait until
    // this request was processed by the server and a response is availabe. Mutex_sem with an initial value 0.
    if ((mutex_sem = sem_open (SEM_MUTEX_NAME, O_CREAT, 0660, 0)) == SEM_FAILED){
		bail_out("sem_open error");
	}
	// Request semaphore. Indicating if there is a request waiting. Initial value = 0
	if ((request_signal_sem = sem_open (SEM_REQUEST_SIGNAL_NAME, O_CREAT, 0660, 0)) == SEM_FAILED){
	   bail_out("sem_open error");
	}
    // Response semaphore. Indicating if the response was printed to the shared memory. Initial value = 0
    if ((response_signal_sem = sem_open (SEM_RESPONSE_SIGNAL_NAME, O_CREAT, 0660, 0)) == SEM_FAILED){
	   bail_out("sem_open error");
	}

	// Set mutex semaphore as 1 to
    // indicate shared memory segment is available
    if (sem_post (mutex_sem) == -1){
		bail_out("sem_post: mutex_sem");
	}

	while (!exit_flag) {
		// Is there a request? P (request_signal_sem);
		if (sem_wait (request_signal_sem) == -1){
			if(exit_flag){
				printf(" Server exiting.\n");
                (void) write_account_list();
				(void) free_resources();
				return EXIT_SUCCESS;
			}
			bail_out("sem_wait: request_signal_sem");
		}

		printf("Client wrote %s\n", shm_ptr -> client_request.operation);
		response = malloc(RESPONSE_SIZE);
		fulfill_request(&(shm_ptr -> client_request), response);

        /* Request has been processed.
		  Release request shared-memory: V (buffer_count_sem);  */
		if (sem_post (mutex_sem) == -1){
            free(response);
		    bail_out("sem_post: buffer_count_sem");
		}

		sprintf(shm_ptr -> response_buffer, "%s", response);
		free(response);

        // Response was printed, tell client
        if (sem_post(response_signal_sem) == -1){
            bail_out("sem_post: response_signal_sem");
        }
	}

	free_resources();
	return EXIT_SUCCESS;
}
