/**
 * @brief: header file for account struct
 * @author: Sabine Weninger
 * @date: 14.12.2016
 */

#include <stdio.h>
#include <stdint.h>
#include <semaphore.h>

#ifndef ACCOUNT
#define ACCOUNT

#define ACCOUNT_NUMBER_LENGTH (7)

typedef struct {
	uint8_t account_number [ACCOUNT_NUMBER_LENGTH];
	long balance;
	pid_t pid;
} account;

/**
* @brief: takes a string respresentation of a account (<account number>;<balance>), parses the account_number and the balance and returns a pointer the
		  newly created account
* @param: pointer to the account string
* @return: the account on success; NULL on error
*/
extern account *parse_account(char *line);

/**
* @brief: takes a string respresentation of a account (<account number>;<balance>), parses the account_number and the balance and returns a pointer the
		  newly created account
* @param: acc: the account to be printed; stream: the stream to which to print the account (stdout or file)
*/
extern void print_account(account *acc, FILE *stream);

/**
* @brief: checks if account_numbers given as acc1 and acc2 are equal.
* @param: acc1, acc2: the account_numbers to compare
* @return: returns 1 if both account_numbers are identical, 0 otherwise
*/
extern int account_numbers_equal(uint8_t *acc1, uint8_t *acc2);

#endif
