/**
 * @brief: implementation of account methods
 * @author: Sabine Weninger
 * @date: 14.12.2016
 */

#include "account.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <limits.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

account *parse_account(char *line){
	int i, j;
	char *ptr, *endptr;
	uint8_t temp;
	long bal_tmp;
	account *acc;
	char delimiter[] = ";";
	//create struct account
	acc = malloc(sizeof(account));
	// initialize strtok and get first section
	ptr = strtok(line, delimiter);
	for(i = 0; i < 2; ++i) {
		if(ptr == NULL){
			free(acc);
			return NULL;
		}
		if(i == 0){
			//read in account number
			for(j = 0; j < ACCOUNT_NUMBER_LENGTH; ++j){
				if(*ptr == '\0'){
					break;
				}
				temp = *ptr - '0';
				if(temp < 0 || temp > 9){
					free(acc);
					return NULL;
				}
				acc->account_number[j] = temp;
				ptr++;
			}
			if(*ptr != '\0'){
				free(acc);
				return NULL;
			}
		}
		if(i == 1){
			//read in balance
		   errno = 0;    /* To distinguish success/failure after call */
		   bal_tmp = strtol(ptr, &endptr, 10);

		   /* Check for various possible errors */
		   if ((errno == ERANGE && (bal_tmp == LONG_MAX || bal_tmp == LONG_MIN))
				   || (errno != 0 && bal_tmp == 0)) {
			   free(acc);
			   return NULL;
		   }
		   if (!(*endptr == '\n' || *endptr == '\0')) {
			   free(acc);
			   return NULL;
		   }
		   /* If we got here, strtol() successfully parsed a number */
		   acc->balance = bal_tmp;
		}
		// create next section
		ptr = strtok(NULL, delimiter);
	}
	if(ptr != NULL){
		free(acc);
		return NULL;
	}
	return acc;
}

void print_account(account *acc, FILE *stream){
	int i, fd_shm;
	for(i = 0; i < ACCOUNT_NUMBER_LENGTH; ++i){
		fprintf(stream, "%d",acc->account_number[i]);
	}
	fprintf(stream, ";%ld\n", acc->balance);
}

int account_numbers_equal(uint8_t *acc1, uint8_t *acc2){
    int i;
    int equal = 1;
    for(i = 0; i < ACCOUNT_NUMBER_LENGTH; ++i){
        if(acc1[i] != acc2[i]){
            equal = 0;
        }
    }
    return equal;
}
