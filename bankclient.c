/**
 * @brief: client implementation of the banken assignment
 * @author: Sabine Weninger
 * @date: 14.12.2016
 */

 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <stdint.h>
 #include <unistd.h>
 #include <signal.h>
 #include <errno.h>
 #include <limits.h>
 #include <sys/mman.h>
 #include <sys/stat.h>
 #include <fcntl.h>
 #include <semaphore.h>
 #include "request.h"



/* === Constants === */

#define ACCOUNT_NUMBER_LENGTH (7)
#define REQUEST_STRING_LENGTH (30)
#define RESPONSE_SIZE (50)
#define SHM_NAME ("/1412-shm")
#define SEM_MUTEX_NAME ("/1412-sem-mutex")
#define SEM_REQUEST_SIGNAL_NAME ("/1412_sem-request-signal")
#define SEM_RESPONSE_SIGNAL_NAME ("/1412_sem-response-signal")



/* === Macros === */

#ifdef ENDEBUG

#define DEBUG(...) do { fprintf(stderr, __VA_ARGS__); } while(0)
#else
#define DEBUG(...)
#endif

/* === Global Struct === */
struct shared_memory{
    request client_request;
    char response_buffer[RESPONSE_SIZE];
};

/* === Global Variables === */
static pid_t pid;
static char *progname = "bankclient";
static char *account_number;
volatile sig_atomic_t exit_flag = 0;
static struct shared_memory *shm_ptr;
static sem_t *mutex_sem, *request_signal_sem, *response_signal_sem;
static request client_request;


/**
* @brief: converts string account_number to array [7] account number
* @param: account_string: pointer to the account string; which: 0: fills account_number in request, 1: fills other_account in request
* @return: 0 on success; -1 on error
*/
int parse_account_number(char *account_string, int which){
	int j, error = 1;
    uint8_t temp;

	for(j = 0; j < ACCOUNT_NUMBER_LENGTH; ++j){
		if(*account_string == '\0'){
			break;
		}
		temp = *account_string - '0';
		if(temp < 0 || temp > 9){
			return -1;
		}
        if(temp != 0){
            //account number != 0000000
            error = 0;
        }
        if(which){
            client_request.other_account[j] = temp;
        }else{
            client_request.account_number[j] = temp;
        }
		account_string++;
	}
	if(*account_string != '\0'){
		return -1;
	}
    if(error){
        return -1;
    }else{
        return 0;
    }
}


/**
* @brief Uses getopt to make sure no options were given
* @param argc the argument counter, argv[] the argument array
*/
static void parse_args(int argc, char *argv[]){
	int opt;

	while ((opt = getopt(argc, argv, "")) != -1) {
		switch (opt) {
			case '?':
				(void) fprintf(stderr, "Usage: %s ACCOUNT_NR\n", argv[0]);
				exit(EXIT_FAILURE);
				break;
			default: /* '?' */
				(void) fprintf(stderr, "Usage: %s ACCOUNT_NR\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	if (argc - optind != 1) {
		(void) fprintf(stderr, "Usage: %s ACCOUNT_NR\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	progname = argv[0];
	account_number = argv[optind];
    if (parse_account_number(account_number, 0) == -1){
        (void) fprintf(stderr, "Usage: %s ACCOUNT_NR(max 7 digits)\n", argv[0]);
        exit(EXIT_FAILURE);
    }
}

/**
* @brief sets the exit_flag to 1 (true), when a SIGINT or SIGTERM Signal is received
*/
void signal_handler() {
  exit_flag = 1;
}

/**
* @brief unmaps shared memories, does not unlink semaphores (this is only done by the server)
*/
void free_resources(){
    printf("Freeing resources!\n");
    int error = 0;
    //unmap shared memory
    if (munmap (shm_ptr, sizeof (struct shared_memory)) == -1){
        (void) fprintf(stderr, "%s: %s\n", progname, "munmap");
        error = 1;
    }
    if(error){
		exit(EXIT_FAILURE);
	}
}

/**
* @brief Prints error message, calls free_resources and exits with FAILURE
* @param message: the error message to be printed
*/
void bail_out(char *message){
	(void) fprintf(stderr, "%s: %s\n", progname, message);
	(void) free_resources();
	exit(EXIT_FAILURE);
}


/**
* @brief: takes a request string, parses operation, value, account_numbers and fills in the gobal request struct accordingly
* @param: request_string: the request string as written by the user
* @return: 0 on success; -1 on error
*/
int parse_request(char *request_string){
    int i, j;
    size_t len;
    char *ptr, *endptr;
    char delimiter[] = " ";
    long amount_tmp;

    //clear client_request
    (void) memset(&client_request, 0, sizeof(request));

    //remove newline from end of request_string
    len = strlen(request_string);
	if (request_string[len - 1] == '\n') {
		request_string[len - 1] = '\0';
	} else {
		return -1;
	}

    //add account_number of this client (given as parameter)
    if(parse_account_number(account_number, 0) == -1){
        return -1;
    }
    // initialize strtok and get first section
	ptr = strtok(request_string, delimiter);

    for(i = 0; i < 3; ++i){
        if(ptr == NULL){
    		return -1;
    	}
        if(i == 0){
            //Check for operation
            if(strcmp(ptr, "!login") == 0){
                strcpy(client_request.operation, "!login");
                ptr = strtok(NULL, delimiter);
                break;
            }else if(strcmp(ptr, "!logout") == 0){
                strcpy(client_request.operation, "!logout");
                ptr = strtok(NULL, delimiter);
                break;
            }else if(strcmp(ptr, "!balance") == 0){
                strcpy(client_request.operation, "!balance");
                ptr = strtok(NULL, delimiter);
                break;
            }else if(strcmp(ptr, "!withdraw") == 0){
                strcpy(client_request.operation, "!withdraw");
            }else if(strcmp(ptr, "!deposit") == 0){
                strcpy(client_request.operation, "!deposit");
            }else if(strcmp(ptr, "!transfer") == 0){
                strcpy(client_request.operation, "!transfer");
            }else{
        		return -1;
            }
        }
        if(i == 1){
            //parse amount
            errno = 0;    /* To distinguish success/failure after call */
            amount_tmp = strtol(ptr, &endptr, 10);

            /* Check for various possible errors */
            if ((errno == ERANGE && (amount_tmp == LONG_MAX || amount_tmp == LONG_MIN))
            	   || (errno != 0 && amount_tmp == 0)) {
               return -1;
            }
            if (!(*endptr == '\n' || *endptr == '\0')) {
               return -1;
            }
            /* If we got here, strtol() successfully parsed a number */
            client_request.value = amount_tmp;
            if(strcmp(client_request.operation, "!transfer") != 0){
                //only !transfer operation needs 2nd argument
                ptr = strtok(NULL, delimiter);
                break;
            }
        }
        if(i == 2){
            if(parse_account_number(ptr, 1) == -1){
                return -1;
            }
        }
    	// create next section
    	ptr = strtok(NULL, delimiter);
    }
    if(ptr != NULL){
		return -1;
	}
    client_request.pid = pid;
    return 0;
}

void communication(char *request_string){
    int req_error;
    req_error = parse_request(request_string);
    if(req_error == -1){
        printf("Malformed request! Please try again.\n");
        return;
    }

    /* There might be multiple clients. We must ensure that
        only one client writes to shared_memory at a time.  */
    // P (mutex_sem);
    if (sem_wait (mutex_sem) == -1){
        bail_out("sem_wait: mutex_sem");
    }

    // Critical section
    memcpy(&(shm_ptr->client_request), &client_request, sizeof(request));

    // Tell server that there is a request to be processed: V (request_signal_sem);
    if (sem_post (request_signal_sem) == -1){
        bail_out("sem_post: (request_signal_sem");
    }

    //Wait for answer from server
    if(sem_wait(response_signal_sem) == -1){
        bail_out("sem_wait: (response_signal_sem)");
    }

    if(strcmp(shm_ptr->response_buffer, "Cannot log in to this account.") == 0){
        bail_out(shm_ptr->response_buffer);
    }
    printf("%s\n", shm_ptr->response_buffer);
}

/**
* @brief Program entry point. Parses the program arguments. Opens shared memories and semaphores. Reads in client requests and
*        parses them into request structs, writes this into the request shared memory and waits for response by server.
* @param arc: argument counter; argv[]: pointer to argument list
*/
int main(int argc, char *argv[]){
	int i;
    int fd_shm;
    char request_string[REQUEST_STRING_LENGTH];
	(void) parse_args(argc, argv);

    pid = getpid();

    /* setup signal handlers */
    const int signals[] = {SIGINT, SIGTERM};
    struct sigaction s;
    s.sa_handler = signal_handler;
    s.sa_flags   = 0;
    if(sigfillset(&s.sa_mask) < 0) {
        bail_out("sigfillset");
    }
    for(i = 0; i < (sizeof(signals) / sizeof(int)); i++) {
        if (sigaction(signals[i], &s, NULL) < 0) {
            bail_out("sigaction");
        }
    }

    // Get shared memory
    if ((fd_shm = shm_open (SHM_NAME, O_RDWR, 0)) == -1){
		bail_out("shm_open");
	}

    if ((shm_ptr = mmap (NULL, sizeof (struct shared_memory), PROT_READ | PROT_WRITE, MAP_SHARED,
            fd_shm, 0)) == MAP_FAILED){
				bail_out("mmap");
			}

    // get semaphores
	//  mutual exclusion semaphore, mutex_sem
    if ((mutex_sem = sem_open (SEM_MUTEX_NAME, 0, 0, 0)) == SEM_FAILED){
		bail_out("sem_open");
	}
    // Request semaphore. Indicating if there is a request waiting. Initial value = 0
    if ((request_signal_sem = sem_open (SEM_REQUEST_SIGNAL_NAME, 0, 0, 0)) == SEM_FAILED){
		bail_out("sem_open");
	}
    // Response semaphore. Indicating if the response was printed to the shared memory. Initial value = 0
    if ((response_signal_sem = sem_open (SEM_RESPONSE_SIGNAL_NAME, 0, 0, 0)) == SEM_FAILED){
	   bail_out("sem_open error");
	}

    //Send login request
    strcpy(request_string, "!login\n");
    (void) communication(request_string);

    // print out Usage
    printf("List of operations:\n");
    printf("   Get account balance: !balance\n");
    printf("   Withdraw money: !withdraw <amount>\n");
    printf("   Deposit money: !deposit <amount>\n");
    printf("   Transfer money: !transfer <amount> <account-no of receiving account>\n");

    while(fgets(request_string, REQUEST_STRING_LENGTH * sizeof(char), stdin) != NULL){
        (void) communication(request_string);
    }

    //Send logout request
    strcpy(request_string, "!logout\n");
    (void) communication(request_string);
    (void) free_resources();
	return EXIT_SUCCESS;
}
